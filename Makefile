all: decaf

decaf: ast.o parser.o lexer.o main.o
	g++ -o $@ $^
	
tree.o: ast.cpp ast.h
	g++ -c -o $@ $<

main.o: main.cpp ast.h
	g++ -c -o $@ $<

lexer.o: lexer.cpp
	g++ -c -o $@ $<

lexer.cpp: lexer.l ast.h
	flex -o $@ $<

parser.cpp: parser.y ast.h
	bison --defines=tokens.h -o $@ $<

tparse.o: parser.cpp
	g++ -c -o $@ $<

clean:
	rm -f *.o
	rm -f lexer.cpp
	rm -f parser.cpp
	rm -f tokens.h
	rm -f tinyc
