#ifndef _AST_H_ 
#define _AST_H_

#include <string>
#include <list>
#include <map>
#include <algorithm>
#include <stdexcept>
using namespace std;

enum DataType {DT_Int, DT_Bool,DT_VOID};

struct VValue {
    bool BoolValue() {
	if (type == DT_Int)
	    return (bool)u.ivalue;
	else
	    return u.bvalue;
    }
    
    int intValue() {
	if (type == DT_Int)
	    return u.ivalue;
	else
	    return (bool)u.bvalue;
    }
    
    DataType type;
    int dimen;
    string place;
    union {
        int ivalue;
        bool bvalue;
    } u;
};

extern map<string, VValue> vars;
extern map<string, VValue> local_params;

//extern map<string, DataType> symbolTable;
enum ExprKind {
  LT_EXPR,
  LTE_EXPR,
  GT_EXPR,
  GTE_EXPR,
  NE_EXPR,
  NEG_EXPR,
  EQ_EXPR,
  ADD_EXPR,
  SUB_EXPR,
  MULT_EXPR,
  DIV_EXPR,
  MOD_EXPR,
  NUM_EXPR,
  ID_EXPR,
  STRING_EXPR,
  ARRAY_EXPR,
  BOOL_EXPR,
  CHAR_EXPR,
  OR_EXPR,
  AND_EXPR,
  ROT_EXPR,
  RSHIFT_EXPR,
  LSHIFT_EXPR,
  METHOD_CALL_EXPR
};

class Expr;
class Statement;
class MethodDeclStatement;
class VarDeclarationStatement;
typedef list<Expr*> ExprList;
extern map<string, Statement*> Methods_def;
typedef list<Statement*> StList;
typedef list<string> idList;
typedef list<VarDeclarationStatement*> DeclList;

class Expr {
public:
    virtual VValue evaluate() = 0;
    virtual string generateCode(string &place) = 0;
    virtual int getKind() = 0;
    bool isA(int kind) { return (getKind() == kind); }
    VValue val;
};

class BinaryExpr: public Expr {
public:
    BinaryExpr(Expr *expr1, Expr *expr2) {
        this->expr1 = expr1;
        this->expr2 = expr2;
    }
    
    Expr *expr1;
    Expr *expr2;
};

class LessThanExpr: public BinaryExpr {
public:
    LessThanExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    string generateCode(string &place);
    VValue evaluate();
    int getKind() { return LT_EXPR; }
};

class GreaterThanExpr: public BinaryExpr {
public:
    GreaterThanExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    string generateCode(string &place);
    VValue evaluate();
    int getKind() { return GT_EXPR; }
};

class LessEqualsExpr: public BinaryExpr {
public:
    LessEqualsExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    string generateCode(string &place);
    VValue evaluate();
    int getKind() { return LTE_EXPR; }
};

class GreaterEqualsExpr: public BinaryExpr {
public:
    GreaterEqualsExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    string generateCode(string &place);
    VValue evaluate();
    int getKind() { return GTE_EXPR; }
};

class NotEqualsExpr: public BinaryExpr {
public:
    NotEqualsExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    string generateCode(string &place);
    VValue evaluate();
    int getKind() { return NE_EXPR; }
};

class EqualsExpr: public BinaryExpr {
public:
    EqualsExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    string generateCode(string &place);
    VValue evaluate();
    int getKind() { return EQ_EXPR; }
};

class AddExpr: public BinaryExpr {
public:
    AddExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    string generateCode(string &place);
    VValue evaluate();
    int getKind() { return ADD_EXPR; }
};

class SubExpr: public BinaryExpr {
public:
    SubExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    string generateCode(string &place);
    VValue evaluate();
    int getKind() { return SUB_EXPR; }
};

class MultExpr: public BinaryExpr {
public:
    MultExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    string generateCode(string &place);
    VValue evaluate();
    int getKind() { return MULT_EXPR; }
};

class DivExpr: public BinaryExpr {
public:
    DivExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    string generateCode(string &place);
    VValue evaluate();
    int getKind() { return DIV_EXPR; }
};

class OrExpr: public BinaryExpr {
public:
    OrExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    string generateCode(string &place);
    VValue evaluate();
    int getKind() { return OR_EXPR; }
};

class AndExpr: public BinaryExpr {
public:
    AndExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    string generateCode(string &place);
    VValue evaluate();
    int getKind() { return AND_EXPR; }
};

class ModExpr: public BinaryExpr {
public:
    ModExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    string generateCode(string &place);
    VValue evaluate();
    int getKind() { return MOD_EXPR; }
};

class RightShiftExpr: public BinaryExpr {
public:
    RightShiftExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    string generateCode(string &place);
    VValue evaluate();
    int getKind() { return RSHIFT_EXPR; }
};

class LeftShiftExpr: public BinaryExpr {
public:
    LeftShiftExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    string generateCode(string &place);
    VValue evaluate();
    int getKind() { return LSHIFT_EXPR; }
};

class RotExpr: public BinaryExpr {
public:
    RotExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}
    string generateCode(string &place);
    VValue evaluate();
    int getKind() { return ROT_EXPR; }
};

class NegExpr: public Expr{
public:
    NegExpr(Expr *expr1){ this->expr1=expr1;}
    string generateCode(string &place);
    VValue evaluate();
    int getKind(){return NEG_EXPR;}
    Expr *expr1;
};

class IntConstantExpr: public Expr {
public:
    IntConstantExpr(int value) { this->value = value; }
    VValue evaluate() { VValue v; v.u.ivalue = value; return v; }
    int getKind() { return NUM_EXPR; }
    string generateCode(string &place);
    int value;
};

class IdExpr: public Expr {
public:
    IdExpr(string id, int dimension) { this->id = id; this->dimension= dimension;}
    VValue evaluate() {  map<string, VValue>::iterator it = local_params.begin();
			if(it != local_params.end()){
				return local_params[id];			
			}else{
				map<string, VValue>::iterator it2 = vars.begin();
				if(it != vars.end()){
					return vars[id];			
				}else{
					throw invalid_argument("Error: Variable has not been declared.");
				}
			}
			
			
		      }
    int getKind() { return ID_EXPR; }
    string generateCode(string &place);
    string id;
    int dimension;
};

class MethodCallExpr: public Expr{
public:
    MethodCallExpr(string id,list<Expr *> params){
        this->id=id;
	this->params=params;
    }
    VValue evaluate();
    int getKind() { return METHOD_CALL_EXPR; }
    list<Expr *> params;
    string generateCode(string &place);
    string id;
};

class ArrayExpr:public Expr{
   ArrayExpr(string id, int position){ this->id=id; 
				       this->index=position;
				     }
   VValue evaluate(){ return vars[id]; } /*TODO: Implementar arrays*/
   int getKind(){return ARRAY_EXPR;}
   string generateCode(string &place);
   string id;
   int index;
};

class StringConstant: public Expr {
public:
    StringConstant(string str) { this->str = str; }
    VValue evaluate() { VValue v; return v; }
    int getKind() { return STRING_EXPR; }
    string generateCode(string &place);
    string str;
};



class BoolConstant: public Expr {
public:
    BoolConstant(bool value) { this->value = value; }
    VValue evaluate() { VValue v; v.u.bvalue = value; return v;}
    int getKind() { return BOOL_EXPR; }
    string generateCode(string &place);
    bool value;
};

class CharConstant: public Expr{
public:
    CharConstant(char *value){this->value=value;}
    VValue evaluate() {VValue v; v.u.ivalue = value[0]; return v;}
    int getKind() { return CHAR_EXPR; }
    string generateCode(string &place);
    char *value;
};

enum StatementKind {
    BLOCK_STATEMENT,
    PRINT_STATEMENT,
    READ_STATEMENT,
    CALL_METHOD_STATEMENT,
    ASSIGN_STATEMENT,
    IF_STATEMENT,
    WHILE_STATEMENT,
    DECLARATION_METHOD_STATEMENT,
    DECLARATION_FIELD_STATEMENT,
    DECLARATION_ASSIGN_STATEMENT,
    PARAM_DECLARATION_STATEMENT,
    VAR_DECLARATION_STATEMENT,
    FOR_STATEMENT,
    BREAK_STATEMENT,
    CONTINUE_STATEMENT,
    RETURN_STATEMENT,
    EMPTY_STATEMENT,
    PROGRAM_STATEMENT
};

class Statement {
public:
    virtual void execute() = 0;
    virtual string generateCode (string label1,string label2) = 0;
    virtual StatementKind getKind() = 0;
};
class ProgramStatement:public Statement {
public:
    ProgramStatement(string id, list<Statement *> declars, list<Statement*> methods )
    {
	this->declars=declars;
	this->methods=methods;
    }
    void execute();
    string generateCode (string label1,string label2);
    StatementKind getKind() { return PROGRAM_STATEMENT; }
    list<Statement *> declars, methods;

};

class EmptyStatement: public Statement {
public:
    EmptyStatement() {  }
    string generateCode (string label1,string label2);
    void execute();
    StatementKind getKind() { return EMPTY_STATEMENT; }
    
    
};

class ReadStatement: public Statement {
public:
    ReadStatement() {  }
    string generateCode (string label1,string label2);
    void execute();
    StatementKind getKind() { return READ_STATEMENT; }
    
    
};

class MethodCallStatement: public Statement {
public:
    MethodCallStatement(string id, list<Expr*> lexpr) { this->id = id; this->lexpr=lexpr; }
    void execute();
    StatementKind getKind() { return PRINT_STATEMENT; }
    string id;
    string generateCode (string label1,string label2);
    list<Expr*> lexpr;
    
};

class ContinueStatement: public Statement {
public:
    ContinueStatement() { }
    void execute();
    string generateCode (string label1,string label2);
    StatementKind getKind() { return CONTINUE_STATEMENT; }
    
    
};


class BreakStatement: public Statement {
public:
    BreakStatement() { }
    void execute();
    string generateCode (string label1,string label2);
    StatementKind getKind() { return BREAK_STATEMENT; }
    
    
};

class ReturnStatement: public Statement {
public:
    ReturnStatement(Expr * expr) { this->expr=expr;}
    void execute();
    string generateCode (string label1,string label2);
    StatementKind getKind() { return RETURN_STATEMENT; }
    Expr *expr;
    
};


class BlockStatement: public Statement {
public:
    BlockStatement(list<Statement *> declVars,list<Statement*> stList) { 
	this->stList = stList; 
	this->declVars = declVars;
	
    }
    void execute();
    StatementKind getKind() { return BLOCK_STATEMENT; }
    list<Statement *> declVars;
    string generateCode (string label1,string label2);
    list<Statement *> stList;
};

class DeclarationFieldStatement: public Statement {
public:
    DeclarationFieldStatement(list<Expr*> ids, DataType type) { 
        this->ids = ids;
        this->type = type; 
    }
    void execute();
    StatementKind getKind() { return DECLARATION_FIELD_STATEMENT; }
    string generateCode (string label1,string label2);
    list<Expr*> ids;
    DataType type;
};

class DeclarationAssignStatement: public Statement {
public:
    DeclarationAssignStatement(DataType type,string id,Expr* value ) { 
        this->id = id;
        this->type = type; 
 	this->value=value;
    }
    void execute();
    StatementKind getKind() { return DECLARATION_ASSIGN_STATEMENT; }
    string generateCode (string label1,string label2);
    string id;
    Expr * value;
    DataType type;
};

class MethodDeclStatement: public Statement {
public:
    MethodDeclStatement(DataType type,string id,list<Statement*>params,Statement *codigo) { 
        this->id = id;
        this->type = type; 
        this->params=params;
        this->codigo=codigo;
    }
   
    void execute();
    StatementKind getKind() { return DECLARATION_ASSIGN_STATEMENT; }
    string generateCode (string label1,string label2);
    string id;
    DataType type;
    list<Statement*>params;
    Statement * codigo;
};

class ParamDeclarationStatement: public Statement {
public:
    ParamDeclarationStatement(DataType type, string id) { 
        this->id = id;
        this->type = type; 
    }
    void execute();
    StatementKind getKind() { return PARAM_DECLARATION_STATEMENT; }
    string generateCode (string label1,string label2);
    string id;
    DataType type;
};


class PrintStatement: public Statement {
public:
    PrintStatement(ExprList lexpr) { this->lexpr = lexpr; }
    void execute();
    StatementKind getKind() { return PRINT_STATEMENT; }
    string generateCode (string label1,string label2);
    ExprList lexpr;
};

class AssignStatement: public Statement {
public:
    AssignStatement(Expr *id, Expr *expr) { 
        this->id = id;
        this->expr = expr; 
    }
    void execute();
    StatementKind getKind() { return ASSIGN_STATEMENT; }
    string generateCode (string label1,string label2);
    Expr *id;
    Expr *expr;
};


class VarDeclarationStatement: public Statement {
public:
    VarDeclarationStatement(DataType datatype, idList list_id) {
	this->datatype = datatype;
	this->list_id = list_id; 
        
    }
    void execute();
    StatementKind getKind() { return VAR_DECLARATION_STATEMENT; }
    string generateCode (string label1,string label2);
    DataType datatype;
    idList list_id;
    
};

class ForStatement: public Statement {
public:
    ForStatement(list<Statement*> assign, Expr * cond,list<Statement*> increment, Statement *codigo) { 
       this->increment = increment;
	   this->cond = cond;
	   this->assign = assign;
	   this->codigo = codigo;
    }
    void execute();
    StatementKind getKind() { return FOR_STATEMENT; }
    list<Statement*> increment;
    list<Statement*> assign;
    Expr *cond;
    Statement *codigo;
    string generateCode (string label1,string label2);
};

class IfStatement: public Statement {
public:
    IfStatement(Expr *cond, Statement *trueBlock, Statement *falseBlock) { 
        this->cond = cond; 
        this->trueBlock = trueBlock;
        this->falseBlock = falseBlock;
    }
    void execute();
    StatementKind getKind() { return IF_STATEMENT; }
    string generateCode (string label1,string label2);
    Expr *cond;
    Statement *trueBlock;
    Statement *falseBlock;
};

class WhileStatement: public Statement {
public:
    WhileStatement(Expr *cond, Statement *block) { 
        this->cond = cond; 
        this->block = block;
    }
    void execute();
    StatementKind getKind() { return WHILE_STATEMENT; }
    string generateCode (string label1,string label2);
    Expr *cond;
    Statement *block;
};



#endif

