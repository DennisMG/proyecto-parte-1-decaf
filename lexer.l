%option noyywrap

%{
#include <cstdlib>
#include <cstring>
#include "ast.h"
#include "tokens.h"


int line = 0;

%}

DIGIT [0-9]
LETTER [a-zA-Z]
ID [a-zA-Z_][a-zA-Z0-9_]*

%%
"bool" { return KW_BOOL;}
"break" { return KW_BREAK;}
"read" {return KW_READ; }
"continue" {return KW_CONTINUE; }
"class" {return KW_CLASS; }
"else" { return KW_ELSE; }
"extends" { return KW_EXTENDS;}
"false" { return KW_FALSE; }
"for" {return KW_FOR; }
"if" {return KW_IF; }
"int" {return KW_INT; }
"new" {return KW_NEW; }
"null" { return KW_NULL; } 
"return" {return KW_RETURN; } 
"rot" { return KW_ROT; }
"true" {return KW_TRUE; } 
"void" { return KW_VOID; }
"while" { return KW_WHILE;}
"print" { return KW_PRINT; }
\"(\\.|[^\\"])*\" { yylval.cadena_t = strdup(yytext);  return CONS_STRING; }
'(\\.|[^\\'])+' { yylval.char_const_t=strdup(yytext); return CONS_CHAR; }
"=" { return ASSIGN; }
"+" { return SUMA; }
"-" { return RESTA; }
"*" { return MULTIPL; }
"/" { return DIVISION; }
"(" { return '('; }
")" { return ')'; }
"{" { return '{'; }
"}" { return '}'; }
"[" { return '['; }
"]" { return ']'; }
","  { return ','; }
"<<" { return LEFT_SHIFT; }
">>" { return RIGHT_SHIFT; }
"<" { return LESS_THAN; }
">" { return GREATER_THAN; }
"%" { return MODULO; }
"<=" { return LESS_EQUALS; }
">=" { return GREATER_EQUALS; }
"==" { return EQUALS; }
"!=" { return NOT_EQUALS; }
"&&" { return OP_AND; }
"||" { return OP_OR; }
"." { return '.'; }
";" { return ';'; }
[ \t\r] /* Nada */
\n  { line++; }
{DIGIT}+ { yylval.num_t = atoi(yytext); return CONS_INT; }
"0x"[0-9A-Fa-f]+ { yylval.num_t = (int)strtol(yytext, NULL, 0); return CONS_INT; }
{ID} { yylval.id_t = strdup(yytext); return ID; }
"//"[^\n]*
. { printf("Simbolo no valido\n"); }
%%
