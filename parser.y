%expect 4
%{

#include <cstdio>
#include <string>
#include <list>
#include "ast.h"
#include "tokens.h"

using namespace std;

extern int line;

int yylex();

void yyerror(const char *str)
{
    printf("Line %d: %s\n", line, str);
}

Statement *program;

#define YYERROR_VERBOSE 1


%}
%union {
    char *id_t;
    char *cadena_t;
    char *char_const_t; 
    int  num_t;
    idList *idList_t;
    Expr *expr_t;
    Statement *statement_t;
    ExprList *exprlist_t;
    StList *stlist_t;
    DataType datatype_t;   

}

%token<datatype_t> KW_INT KW_BOOL
%token<cadena_t> CONS_STRING
%token<char_const_t> CONS_CHAR
%token<num_t> CONS_INT
%token<id_t> ID
%token  KW_BREAK KW_READ KW_CONTINUE KW_CLASS KW_ELSE KW_EXTENDS KW_FALSE KW_FOR KW_IF  KW_NEW KW_NULL KW_RETURN KW_TRUE KW_VOID KW_WHILE KW_PRINT
%token SUMA RESTA MULTIPL DIVISION NEG_LOG MODULO ASSIGN
%token RIGHT_SHIFT LEFT_SHIFT KW_ROT 
%token LESS_EQUALS GREATER_EQUALS GREATER_THAN LESS_THAN EQUALS NOT_EQUALS
%token OP_AND OP_OR

%left OP_OR
%left OP_AND
%left EQUALS NOT_EQUALS
%left LESS_EQUALS LESS_THAN GREATER_EQUALS GREATER_THAN 
%left LEFT_SHIFT RIGHT_SHIFT KW_ROT
%left MODULO 
%left SUMA RESTA
%left MULTIPL DIVISION
%right NEG_LOG
%left UMINUS

%type<datatype_t> type
%type<idList_t> list_id  
%type<expr_t> expr return_expr argument constant Ivalue  
%type<statement_t>  st  block opt_else method_call assign vardecl fieldDecl methodDecl param program
%type<exprlist_t> list_expr list_exprP argumentList IvalueList varList  
%type<num_t> IvalueP
%type<stlist_t> paramsList List_methodDecl list_vardecl_OPT list_vardecl list_assign list_st_OP List_fieldDecl_OPT List_methodDecl_OPT List_fieldDecl paramsListP list_st

%%
program: KW_CLASS ID '{' List_fieldDecl_OPT  List_methodDecl_OPT '}'  { program=new ProgramStatement($2,*$4,*$5);}
;
List_fieldDecl_OPT:List_fieldDecl		{ $$=$1;}
		  |/*empty list*/		{ $$= new list<Statement *>();}
;
List_fieldDecl: List_fieldDecl fieldDecl	{ $$ = $1; $$->push_back($2); }
	      | fieldDecl	             {$$=new list<Statement*>();$$->push_back($1); }		 	
	      
;
List_methodDecl_OPT: List_methodDecl		{$$=$1;}
		   | /*empty list*/		{$$=new list<Statement*>();}
;
List_methodDecl: List_methodDecl methodDecl	{ $$ = $1; $$->push_back($2);}
		
	       | methodDecl			{$$=new list<Statement*>();$$->push_back($1);}
;	       

fieldDecl: type varList ';'			{ $$ = new DeclarationFieldStatement(*$2,$1); }
	 | type ID ASSIGN constant ';'		{$$ = new DeclarationAssignStatement($1,$2,$4); }
;



varList: Ivalue					{ $$= new list<Expr*>(); $$->push_back($1); }
       | varList ',' Ivalue			{ $$ = $1; $$->push_back($3); }
;

Ivalue: ID IvalueP		{ $$=new IdExpr($1, $2); }
     
;
IvalueP: '[' CONS_INT ']'	{ $$=$2;}
	|/*empty*/ 		{ $$=0;}
      
;
IvalueList: Ivalue		{  $$=new list<Expr*>(); $$->push_back($1);}
	  | IvalueList ',' Ivalue { $$=$1; $$->push_back($3);}
;

type: KW_INT					{$$=DT_Int;}
    | KW_BOOL					{$$=DT_Bool;}
;

methodDecl: KW_VOID ID '(' paramsList ')' block	{string id = $2; free($2); $$=new MethodDeclStatement(DT_VOID,id,*$4,$6);}
	  | type ID '(' paramsList ')' block	{ $$=new MethodDeclStatement($1,$2,*$4,$6);}
;

paramsList: paramsListP			{  $$=$1;}
	  | /*empty list*/ 		{ $$= new list<Statement*>();}
	    
;

paramsListP:param			{ 
   					  $$ = new list<Statement*>();
	    				  $$->push_back($1);
					}
	  | paramsListP ',' param	{
					  $$ = $1;
	    			 	  $$->push_back($3);
					}
	    
;

param: type ID				{$$=new ParamDeclarationStatement($1,$2);}
;

block: '{' list_vardecl_OPT list_st_OP '}' {$$=new BlockStatement(*$2,*$3);}
;

list_vardecl_OPT: list_vardecl		{$$=$1;}
		| /* empty*/		{$$=new list<Statement*>();}
;
list_vardecl: vardecl			{$$=new list<Statement*>(); $$->push_back($1);}
	    | list_vardecl vardecl	{$$=$1; $$->push_back($2);}
	  
;

vardecl: type list_id ';'		{$$=new VarDeclarationStatement($1,*$2); }
;

list_id: ID 				{ 
					  $$ = new idList;
					  string id = $1;
					  free($1);
					  $$->push_back(id);
					}		
      | list_id ',' ID 			{
					  $$ = $1;
					  string id = $3;
					  free($3);
					  $$->push_back(id);
					}
;

list_st_OP:/*empty list*/		{ $$=new list<Statement*>();}
	 | list_st 			{ $$=$1;}
;

list_st: st				{ $$=new list<Statement*>(); $$->push_back($1); }
      | list_st st			{ $$=$1; $$->push_back($2); }
;

st: assign ';'				{ $$=$1;}
  | method_call ';'			{ $$=$1;}
  | KW_IF '(' expr ')' block opt_else	{ $$=new IfStatement($3,$5,$6);}
  | KW_WHILE '(' expr ')' block		{ $$= new WhileStatement($3,$5);}
  | KW_FOR '(' list_assign ';' expr ';' list_assign ')' block	{$$=new ForStatement(*$3,$5,*$7,$9);}
  | KW_RETURN return_expr ';'		{ $$=new ReturnStatement($2);}
  | KW_BREAK ';'			{ $$=new BreakStatement();}
  | KW_CONTINUE ';'			{ $$=new ContinueStatement();}
;



return_expr: expr 		{ $$=$1;}
	   | /*epsilon*/		{ $$=0; }
;
list_expr: list_exprP			{ $$=$1;}
	| /*empty List*/		{ $$=new list<Expr*>(); }
;

list_exprP: list_exprP ','expr		{ $$=$1;$$->push_back($3);}
	| expr				{ $$=new list<Expr*>(); $$->push_back($1);}
;

list_assign: list_assign assign		{$$=$1; $$->push_back($2);}
	  | assign			{ $$=new list<Statement*>(); $$->push_back($1);}
;

opt_else: /*epsilon*/			{$$=0;}
        | KW_ELSE block			{$$=$2;}
;
method_call:KW_PRINT argumentList 	{ $$=new PrintStatement(*$2);}
	   | KW_READ IvalueList  	{ $$=new ReadStatement();}
;

argumentList: argument			{ $$=new list<Expr*>(); $$->push_back($1);}
	    |argumentList ',' argument	{ $$=$1;$$->push_back($3);}
;

argument:CONS_STRING			{string literalStr = $1;
            				free($1);$$=new StringConstant(literalStr);}
	|expr				{$$=$1;}
; 
assign: Ivalue ASSIGN expr 	{ $$ = new AssignStatement($1,$3);}
;

expr: constant			{ $$=$1;}
    | Ivalue			{ $$=$1;}
    | ID '(' list_expr ')'	{ $$=new MethodCallExpr($1,*$3);} //method_call 
    | expr OP_OR expr  		{ $$=new OrExpr($1,$3); }
    | expr OP_AND expr  	{ $$=new AndExpr($1,$3);}
    | expr EQUALS expr 		{ $$=new EqualsExpr($1,$3);}
    | expr NOT_EQUALS expr	{ $$=new NotEqualsExpr($1,$3);}
    | expr LESS_THAN expr 	{ $$=new LessThanExpr($1,$3);}
    | expr LESS_EQUALS expr    	{ $$=new LessEqualsExpr($1,$3);}
    | expr GREATER_EQUALS expr  { $$=new GreaterEqualsExpr($1,$3);}
    | expr GREATER_THAN expr	{ $$=new GreaterThanExpr($1,$3);}
    | expr LEFT_SHIFT expr	{ $$=new LeftShiftExpr($1,$3);}
    | expr RIGHT_SHIFT expr	{ $$=new RightShiftExpr($1,$3);}
    | expr KW_ROT expr		{ $$=new RotExpr($1,$3);}
    | expr MODULO expr		{ $$=new ModExpr($1,$3);}
    | expr SUMA expr		{ $$=new AddExpr($1,$3);}
    | expr RESTA expr		{ $$=new SubExpr($1,$3);}
    | expr MULTIPL expr		{ $$=new MultExpr($1,$3);}
    | expr DIVISION expr    	{ $$=new DivExpr($1,$3);}
    | '(' expr ')'		{ $$=$2;}
    | NEG_LOG expr		{ $$=new NegExpr($2);}
    | RESTA expr %prec UMINUS	{ $$=new MultExpr(new IntConstantExpr(-1),$2); }

    
;

constant: KW_TRUE 	{ $$=new BoolConstant(true);  }
    | KW_FALSE 		{ $$=new BoolConstant(false); }
    | CONS_INT 		{ $$=new IntConstantExpr($1);     }
    | CONS_CHAR 	{ $$=new CharConstant($1);    }
    //| KW_NULL		{ $$=new NullConstant($1);    }
;

%%

