#include <cstdio>
#include "ast.h"
#include <sstream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <iostream>
using namespace std;

const char *temps[] = {"$t0", "$t1", "$t2", "$t3", "$t4", "$t5", "$t6", "$t7", "$t8", "$t9"};
#define TEMP_COUNT (sizeof(temps)/sizeof(temps[0]))

map<string, VValue> vars;
map<string, int> tempList;
map<string, Statement * > Methods_def;
map<string, VValue> local_params;
int label;

string newIntTemp() {
    int i;

    for (i = 0; i < TEMP_COUNT; i++) {
        if (tempList.find(temps[i]) == tempList.end()) {
            tempList[temps[i]] = 1;

            return string(temps[i]);
        }
    }

    return string("");
}

string newLabel() {
    string s = "Label";
    stringstream sstm;
    sstm << s << label++;
    return sstm.str();
}

void releaseTemp(string temp) {
    tempList.erase(temp);
}

#define IMPLEMENT_RELATIONAL_EVALUATE(CLASS, OP)   \
    VValue CLASS::evaluate()            \
    {                                   \
        VValue value1 = expr1->evaluate();  \
        VValue value2 = expr2->evaluate();  \
        VValue result;                      \
                                            \
        result.type = DT_Bool;               \
	result.u.bvalue = value1.u.bvalue OP value2.u.bvalue; \
		       \
        return result; \
    }
    
#define IMPLEMENT_ARITHMETIC_EVALUATE(CLASS, OP)    \
    VValue CLASS::evaluate()                \
    {                                       \
        VValue value1 = expr1->evaluate();  \
        VValue value2 = expr2->evaluate();  \
        VValue result;                      \
                                            \
        result.type = DT_Int;                                   \
        result.u.ivalue = value1.u.ivalue OP value2.u.ivalue;   \
	                  \
        return result;    \
    }    
    
IMPLEMENT_RELATIONAL_EVALUATE(LessThanExpr, <)
IMPLEMENT_RELATIONAL_EVALUATE(LessEqualsExpr, <=)
IMPLEMENT_RELATIONAL_EVALUATE(GreaterThanExpr, >)
IMPLEMENT_RELATIONAL_EVALUATE(GreaterEqualsExpr, >=)
IMPLEMENT_RELATIONAL_EVALUATE(NotEqualsExpr, !=)
IMPLEMENT_RELATIONAL_EVALUATE(EqualsExpr, ==)
IMPLEMENT_RELATIONAL_EVALUATE(OrExpr, ||)
IMPLEMENT_RELATIONAL_EVALUATE(AndExpr, &&)

IMPLEMENT_ARITHMETIC_EVALUATE(AddExpr, +)
IMPLEMENT_ARITHMETIC_EVALUATE(SubExpr, -)
IMPLEMENT_ARITHMETIC_EVALUATE(MultExpr, *)
IMPLEMENT_ARITHMETIC_EVALUATE(DivExpr, /)    
IMPLEMENT_ARITHMETIC_EVALUATE(RotExpr, %) 
IMPLEMENT_ARITHMETIC_EVALUATE(ModExpr, %) 
IMPLEMENT_ARITHMETIC_EVALUATE(RightShiftExpr,>>) 
IMPLEMENT_ARITHMETIC_EVALUATE(LeftShiftExpr,<<)



VValue NegExpr::evaluate()
{
	VValue value1 = expr1->evaluate();
	VValue result;
	result.type=DT_Bool;
	result.u.bvalue = value1.u.bvalue;
	return result;
	
}

void ProgramStatement::execute(){
	list<Statement *>::iterator it_d = declars.begin();
	list<Statement *>::iterator it_m = methods.begin();

	while (it_d != declars.end()) {
	    Statement *st = *it_d;
            st->execute();
            it_d++;
	}


	while (it_m != methods.end()) {
	    Statement *st = *it_m;
            st->execute();
            it_m++;
	}

	it_m = methods.begin();
	while (it_m != methods.end()) {
	    Statement *st = *it_m;
            if(((MethodDeclStatement*)st)->id == "main"){

		Statement *cod = ((MethodDeclStatement*)st)->codigo;
		cod->execute();
		return;
		}
            it_m++;	
	}

	printf("Error: no main\n");

	

	
	
}

void BlockStatement::execute(){

    list<Statement *>::iterator it = declVars.begin();
    
    while (it != declVars.end()) {
        Statement *dcl = *it;
        
        dcl->execute();
        it++;
    } 

    list<Statement *>::iterator it2 = stList.begin();
    
    while (it2 != stList.end()) {
        Statement *st = *it2;
        
        st->execute();
        it2++;
    }   
}

void MethodDeclStatement::execute(){
	//Statement * temp= new MethodDeclStatement(type, id,params,codigo);
	//Methods_def.insert(make_pair(id,new MethodDeclStatement(type, id,params,codigo)));
	Methods_def[id]=this;
	
}

void AssignStatement::execute(){

	map<string,VValue>::iterator it = local_params.find(((IdExpr*)id)->id);
	if(it == local_params.end())
	{
		map<string,VValue>::iterator it2 = vars.find(((IdExpr*)id)->id);
		if(it2 == vars.end())
		{
   			throw invalid_argument("Error: variable \""+ ((IdExpr*)id)->id +"\" has not been declare\n");
		}
		vars[((IdExpr*)id)->id]=expr->evaluate();
   		
	}else{
	local_params[((IdExpr*)id)->id]=expr->evaluate();
	}
}

void ParamDeclarationStatement::execute(){
        VValue v; v.type=type;
	local_params[id] = v;

}

void MethodCallStatement::execute()
{

}

void ReturnStatement::execute()
{

}

void BreakStatement::execute()
{

}

void ContinueStatement::execute()
{

}

void PrintStatement::execute() {
    list<Expr *>::iterator it = lexpr.begin();

    while (it != lexpr.end()) {
      Expr *expr = *it;
      
      if (expr->isA(STRING_EXPR)) {
	printf("%s\n", ((StringConstant*)expr)->str.c_str());
      } else if(expr->evaluate().type==DT_Int) {
	VValue result = expr->evaluate();
	printf("%d\n", result.intValue());
      } else if(expr->evaluate().type==DT_Bool){
	printf("%s\n", expr->evaluate().BoolValue() ? "true" : "false");
	}else{
	VValue result = expr->evaluate();
	printf("%d\n", result.intValue());
	
	}
      
      it++;
    }

}

VValue MethodCallExpr::evaluate(){
	  VValue v;
	  map<string,Statement*>::iterator it = Methods_def.find(id);

	if(it == Methods_def.end())
		throw invalid_argument("Error: method "+id+" doesn't exist!");

	if(((MethodDeclStatement*)it->second)->params.size() != params.size()){
		throw invalid_argument("Error: different number of arguments");
	}
	list<Expr*>::iterator it_l = params.begin();
	
	list<Statement*> list_arg = ((MethodDeclStatement*)it->second)->params;
	list<Statement*>::iterator it_p = list_arg.begin();

	
	while(it_l!=params.end()){ ((ParamDeclarationStatement*)*it_p)->execute(); it_l++;}
	it_l=params.begin();

	while (it_l != params.end() && it_p != list_arg.end() ) {
			//local_params[]
	
                
    	  it_l++; it_p++;
    	}
	
	return v;
}

void VarDeclarationStatement::execute(){

    list<string>::iterator it = list_id.begin();
    
    VValue _default;
    _default.type = datatype; 
    _default.u.ivalue=0;
    _default.dimen=0;

    while (it != list_id.end()) {

	map<string,VValue>::iterator it2 = vars.find(*it);
	
    if(it2 != vars.end())
	{
   		throw invalid_argument("Error: variable \""+ *it +"\" has already been declared\n");
	}
 
      vars[*it] = _default;
                
      it++;
    }

}

void DeclarationFieldStatement::execute(){

    list<Expr*>::iterator it = ids.begin();
    
    VValue _default;
    _default.type = type; 
    _default.u.ivalue=0;
    _default.dimen=((IdExpr*)*it)->dimension;

    while (it != ids.end()) {
      string id = ((IdExpr*)*it)->id;
      vars[id] = _default;
                
      it++;
    }
}

void ForStatement::execute(){
    list<Statement*>::iterator it = assign.begin();
	while (it != assign.end()) {
      		(*it)->execute();               
        it++;
    }

	for(;cond->evaluate().BoolValue();){
		codigo->execute();
		
		list<Statement*>::iterator it2 = increment.begin();
		while (it2 != increment.end()) {
      			(*it2)->execute();              
        		it2++;
    		}


	}
	
}


void IfStatement::execute(){
    VValue result = cond->evaluate();

    if(result.type == DT_Bool)
       if (result.u.bvalue) {
          trueBlock->execute();
       } else if (falseBlock != 0) {
          falseBlock->execute();
       }
    else 
	printf("Error: if condition has to be of type Boolean");

}

void WhileStatement::execute(){
  VValue result = cond->evaluate();
  
  while (result.u.bvalue) {
    block->execute();
    
    result = cond->evaluate();
  }
}

void ReadStatement::execute(){
	
}

void DeclarationAssignStatement::execute(){
	
	VValue v;
	v.type = type;
	v.u.ivalue=value->evaluate().u.ivalue;
	/*if(type == DT_Bool)
	   v.u.bvalue=value->evaluate().u.bvalue;
	else if( type == DT_Int)
           v.u.ivalue=value->evaluate().u.ivalue;*/
	
	vars[id]=v;
}



//-------------------------------------------------------------

string LessThanExpr::generateCode(string &place) {
    string place1, place2;
    string code1 = expr1->generateCode(place1);
    string code2 = expr2->generateCode(place2);
    stringstream ss;

    releaseTemp(place1);
    releaseTemp(place2);
    place = newIntTemp();

    ss << code1 << endl <<
            code2 << endl <<
            "slt " << place << ", " << place1 << ", " << place2;

    return ss.str();
}

string GreaterThanExpr::generateCode(string &place) {
    string place1, place2;
    string code1 = expr1->generateCode(place1);
    string code2 = expr2->generateCode(place2);
    stringstream ss;

    releaseTemp(place1);
    releaseTemp(place2);
    place = newIntTemp();

    ss << code1 << endl <<
            code2 << endl <<
            "slt " << place << ", " << place2 << ", " << place1;

    return ss.str();
}

string LessEqualsExpr::generateCode(string &place)//falta
{
    string place1, place2;
    string code1 = expr1->generateCode(place1);
    string code2 = expr2->generateCode(place2);
    stringstream ss;

    releaseTemp(place1);
    releaseTemp(place2);
    place = newIntTemp();

    ss << code1 << endl <<
            code2 << endl <<
            "slt " << place << ", " << place2 << ", " << place1 << "\n" <<
            "nor " << place << ", " << place << ", " << place << "\n" <<
            "addi " << place << ", " << place << ", 2";

    return ss.str();
}

string GreaterEqualsExpr::generateCode(string &place)//falta
{
    string place1, place2;
    string code1 = expr1->generateCode(place1);
    string code2 = expr2->generateCode(place2);
    stringstream ss;

    releaseTemp(place1);
    releaseTemp(place2);
    place = newIntTemp();

    ss << code1 << endl <<
            code2 << endl <<
            "slt " << place << ", " << place1 << ", " << place2 << "\n" <<
            "nor " << place << ", " << place << ", " << place << "\n" <<
            "addi " << place << ", " << place << ", 2";

    return ss.str();
}

string NotEqualsExpr::generateCode(string &place)//falta
{
    string place1, place2, true_l, end_l;
    string code1 = expr1->generateCode(place1);
    string code2 = expr2->generateCode(place2);
    stringstream ss;
    true_l = newLabel();
    end_l = newLabel();

    releaseTemp(place1);
    releaseTemp(place2);
    place = newIntTemp();

    ss << code1 << endl <<
            code2 << endl <<
            "beq " << place1 << ", " << place2 << ", " << true_l << "\n" <<
            "li " << place << ", " << "1\n" <<
            "j " << end_l << "\n" <<
            true_l << ":\n" <<
            "li " << place << ", " << "0\n" <<
            end_l << ":\n";

    return ss.str();
}

string EqualsExpr::generateCode(string &place)
{
    string place1, place2, true_l, end_l;
    string code1 = expr1->generateCode(place1);
    string code2 = expr2->generateCode(place2);
    stringstream ss;
    true_l = newLabel();
    end_l = newLabel();

    releaseTemp(place1);
    releaseTemp(place2);
    place = newIntTemp();

    ss << code1 << endl <<
            code2 << endl <<
            "beq " << place1 << ", " << place2 << ", " << true_l << "\n" <<
            "li " << place << ", " << "0\n" <<
            "j " << end_l << "\n" <<
            true_l << ":\n" <<
            "li " << place << ", " << "1\n" <<
            end_l << ":\n";

    return ss.str();
}


string AddExpr::generateCode(string &place) {
    string place1, place2;
    string code1 = expr1->generateCode(place1);
    string code2 = expr2->generateCode(place2);
    stringstream ss;

    releaseTemp(place1);
    releaseTemp(place2);
    if(expr1->val.type==DT_Int && expr2->val.type==DT_Int){
        place = newIntTemp();

        ss << code1 << endl <<
                code2 << endl <<
                "add " << place << ", " << place1 << ", " << place2;
    }
    return ss.str();
}


string SubExpr::generateCode(string &place) {
    string place1, place2;
    string code1 = expr1->generateCode(place1);
    string code2 = expr2->generateCode(place2);
    stringstream ss;

    releaseTemp(place1);
    releaseTemp(place2);
    place = newIntTemp();

    ss << code1 << endl <<
            code2 << endl <<
            "sub " << place << ", " << place1 << ", " << place2;

    return ss.str();
}

string MultExpr::generateCode(string &place) {
    string place1, place2;
    string code1 = expr1->generateCode(place1);
    string code2 = expr2->generateCode(place2);
    stringstream ss;

    releaseTemp(place1);
    releaseTemp(place2);
    place = newIntTemp();

    ss << code1 << endl <<
            code2 << endl <<
            "mult " << place1 << ", " << place2 << endl <<
            "mflo " << place;

    return ss.str();
}

string DivExpr::generateCode(string &place) {
    string place1, place2;
    string code1 = expr1->generateCode(place1);
    string code2 = expr2->generateCode(place2);
    stringstream ss;

    releaseTemp(place1);
    releaseTemp(place2);
    place = newIntTemp();

    ss << code1 << endl << code2 << endl <<
            "div " << place1 << ", " << place2 << endl <<
            "mflo " << place;

    return ss.str();
}

string OrExpr::generateCode(string &place) {
    stringstream ss;
    string place1, place2;
    string code1 = expr1->generateCode(place1);
    string code2 = expr2->generateCode(place2);

    releaseTemp(place1);
    releaseTemp(place2);
    place = newIntTemp();

    ss << code1 << endl << code2 << endl<<
        "or " << place << place1 << ", " << place2 <<endl;

    return ss.str();
}

string AndExpr::generateCode(string &place) {
    stringstream ss;
    string place1, place2;
    string code1 = expr1->generateCode(place1);
    string code2 = expr2->generateCode(place2);

    releaseTemp(place1);
    releaseTemp(place2);
    place = newIntTemp();

    ss << code1 << endl << code2 << endl <<
        "and " << place << ", " << place1 << ", " << place2 << endl;

    return ss.str();
}

string ModExpr::generateCode(string &place) {
    stringstream ss;
    string place1, place2;
    string code1 = expr1->generateCode(place1);
    string code2 = expr2->generateCode(place2);

    releaseTemp(place1);
    releaseTemp(place2);
    place = newIntTemp();

    ss << code1 << endl << code2 << endl<<
        "div " << place1 << ", "<< place2 << endl <<
        "mfhi "<< place << endl;
    return ss.str();
}

string RightShiftExpr::generateCode(string &place) {
    stringstream ss;
    
    return ss.str();
}

string LeftShiftExpr::generateCode(string &place) {
    stringstream ss;
    
    return ss.str();
}

string RotExpr::generateCode(string &place) {
    stringstream ss;
    
    return ss.str();
}

string NegExpr::generateCode(string &place) {
    stringstream ss;
    
    return ss.str();
}

string IntConstantExpr::generateCode(string &place) {
    stringstream ss;
    
    place = newIntTemp();
    ss << "li " << place << ", " << value<<endl;
    
    return ss.str();
}

string IdExpr::generateCode(string &place) {
    stringstream ss;
    if (vars.find(id) == vars.end()) {
        printf("Id %s no existe!\n", id.c_str());
        exit(0);
    }
    place = newIntTemp();
    ss << "la " << place << ", " << id << endl <<
          "lw " << place << ", " << "0(" << place << ")" << endl;
       
    vars[id].place=place;
    return ss.str();
}

string MethodCallExpr::generateCode(string &place) {
    stringstream ss;
       
    return ss.str();
}

string StringConstant::generateCode(string &place) {
    stringstream ss;
   
    return ss.str();
}

string BoolConstant::generateCode(string &place) {
    stringstream ss;
    place = newIntTemp();
    ss << "li " << place << ", " << value;
    return ss.str();
}

string CharConstant::generateCode(string &place) {
    stringstream ss;
       
    return ss.str();
}
//-----------------STATEMENTS--------------------------
string ProgramStatement::generateCode (string label1,string label2){
    stringstream ss;
       
    return ss.str();
}


string ReadStatement::generateCode (string label1,string label2){
    stringstream ss;
       
    return ss.str();
}

string MethodCallStatement::generateCode (string label1,string label2){
    stringstream ss;
       
    return ss.str();
}


string ContinueStatement::generateCode (string label1,string label2){
    stringstream ss;
       
    return ss.str();
}

string BreakStatement::generateCode (string label1,string label2){
    stringstream ss;
       
    return ss.str();
}

string ReturnStatement::generateCode (string label1,string label2){
    stringstream ss;
       
    return ss.str();
}



string BlockStatement::generateCode (string label1,string label2){
    stringstream ss;

    list<Statement *>::iterator it = declVars.begin();
    
    while (it != declVars.end()) {
        Statement *dcl = *it;
        
        ss << dcl->generateCode(label1,label2);
        it++;
    } 

    list<Statement *>::iterator it2 = stList.begin();
    
    while (it2 != stList.end()) {
        Statement *st = *it2;
        
        ss << st->generateCode(label1,label2);
        it2++;
    }   
       
    return ss.str();
}


string DeclarationFieldStatement::generateCode (string label1,string label2){
    stringstream ss;
       
    return ss.str();
}


string DeclarationAssignStatement::generateCode (string label1,string label2){
    stringstream ss;
       
    return ss.str();
}

string MethodDeclStatement::generateCode (string label1,string label2){
    stringstream ss;
       
    return ss.str();
}

string ParamDeclarationStatement::generateCode (string label1,string label2){
    stringstream ss;
       
    return ss.str();
}

string PrintStatement::generateCode (string label1,string label2){
    stringstream ss;

    ss<<"#--------Print---------"<<endl;

    string place1;

    ExprList::iterator it=lexpr.begin();
    string code="";
    while(it!=lexpr.end()){
         Expr *e = *it; 
         code = e->generateCode(place1); 
         releaseTemp(place1);
         ss << code << endl << 
                "move $a0, " << place1 << endl<< 
                "li $v0, 1" << endl << 
                "syscall" << endl << 
                "la $a0, newLine" << endl << 
                "li $v0, 4" << endl << 
                "syscall" << endl;
        it++;
    }
       
    return ss.str();
}

string AssignStatement::generateCode (string label1,string label2){
    stringstream ss;
    // if (vars.find(id) == vars.end()) {
    //     printf("Variable doesn't exist.\n");
    //     exit(0);
    // }

    // string place1;
    // string code = expr->generateCode(place1);
    // string place ="";

    // place=newIntTemp();
    // releaseTemp(place1);
    // ss << code << endl << "la " << place << ", " << id << endl <<
    //         "sw " << place1 << ", " << "0(" << place << ")" << endl;
    // releaseTemp(place);

    return ss.str();
}
string VarDeclarationStatement::generateCode (string label1,string label2){
    stringstream ss;

    idList::iterator it=list_id.begin();
    
    while(it!=list_id.end()){
        string id=*it;
        if (vars.find(id) != vars.end()) {
            printf("Variable %s has already been declared.\n",id.c_str());
            exit(0);
        }
        VValue v;
        v.type=datatype;
        vars[id] = v;
        ss << id << ": .word 0" << endl;
        
        
        it++;
    }
       
    return ss.str();
}
string ForStatement::generateCode (string label1,string label2){
    stringstream ss;
    ss << "#-----FOR-----"<<endl;
    string place1;
    list<Statement *>::iterator it = assign.begin();
    label1 = newLabel();
    label2 = newLabel();
    stringstream as;

    while (it != assign.end()) {
        Statement *dcl = *it;        
        as << dcl->generateCode(label1,label2)<<endl;
        it++;
    } 
    
    list<Statement *>::iterator it2 = increment.begin();
    stringstream incr;
    while (it2 != increment.end()) {
        Statement *dcl = *it2;        
        incr << dcl->generateCode(label1,label2) << endl;
        it2++;
    }

    string code = cond->generateCode(place1);
    string block = codigo->generateCode(label1, label2);
    releaseTemp(place1);

    ss << as << endl <<
    label1 << ": " << endl <<
    code << endl <<
    "beq " << place1 << ", $zero, "<< label2 << endl <<
    block << endl << 
    incr <<
    "j " << label1<<endl<<

    label2<<": "<<endl;

    ss<< "#-----GOODBYE_FOR-----" <<endl;


    
    return ss.str();
}
string IfStatement::generateCode (string label1,string label2){
    stringstream ss;
    ss<<"#------IF-------"<<endl;
    string place1, place2;
    string code = cond->generateCode(place1);

    string true_lbl = newLabel();
    string endif = newLabel();
    
    place2 = newIntTemp();
    string place = newIntTemp();
    releaseTemp(place1);

    string bcode = trueBlock->generateCode(label1, label2);
    string falseCode = "";
    if (falseBlock != 0)
        falseCode = falseBlock->generateCode(label1, label2);

    ss << code << endl <<
    "addi " << place2 << ", " << "$zero, 1" << endl <<
    "beq " << place1 << ", " << place2 << ", " << true_lbl << endl <<
    falseCode << endl <<
    "j " << endif << endl <<
    true_lbl << ": " << endl <<
    bcode << endl <<
    endif << ": "<<endl;

    releaseTemp(place);

    ss<<"#------GOODBYE_IF-------"<<endl;
    return ss.str();
}

string WhileStatement::generateCode (string label1,string label2){
    stringstream ss;
    ss<<"#----WHILE----" << endl;
    string place1;
    string code = cond->generateCode(place1);
    label1 = newLabel();
    label2 = newLabel();
    releaseTemp(place1);

    string bcode = block->generateCode(label1, label2);

    ss << label1 <<":"<<endl<<
    code << endl <<
    "beq " << place1 << ", " << "$zero, " << label2 << endl <<
    bcode <<
    "j " << label1 << endl <<
    label2 << ":"<<endl;
    ss<< "#------GOODBYE_WHILE------" <<endl;
    return ss.str();
}
