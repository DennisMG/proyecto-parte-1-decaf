#include <cstdio>
#include "ast.h"
#include "tokens.h"
#include <iostream>
#include <map>

using namespace std;
int yyparse();
extern Statement *program;
DeclList *declarationList;

int main()
{
    program = NULL;
    yyparse();
    string lbl1, lbl2;
    
    cout << ".data" << endl;
    cout<< "newLine: .asciiz \"\\n\"" << endl; 
    if(declarationList!=0){
        DeclList::iterator it=declarationList->begin();
        while(it!=declarationList->end()){
            VarDeclarationStatement *d=*it;
            d->generateCode(lbl1,lbl2);
            cout<<endl;
            it++;
        }
    }
    
    if (program != 0) {
        string label1,label2;
        string code = program->generateCode(label1,label2);
        
            cout << ".text"<<endl << code << endl;
            cout << "# Exit"<<endl
                    << "li $v0, 10"<<endl
                    << "syscall"<<endl;
    }
}

